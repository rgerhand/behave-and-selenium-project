# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re


class AppDynamicsJob(unittest.TestCase):
    def setUp(self):
        # AppDynamics will automatically override this web driver
        # as documented in https://docs.appdynamics.com/display/PRO44/Write+Your+First+Script
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.google.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_app_dynamics_job(self):
        driver = self.driver
        driver.get("https://www.comarch.pl/")
        driver.find_element_by_xpath("//aside/div/div[2]").click()
        driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div[2]/ul").click()
        driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div[2]/ul/span[4]").click()
        driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div[2]/ul/span[5]").click()
        driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div/i[2]").click()
        driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div/i").click()
        driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div/i").click()
        # ERROR: Caught exception [ERROR: Unsupported command [doubleClick | //div[@id='widget-6']/div/div/div/i | ]]
        driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div/i").click()
        driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div/i").click()
        driver.find_element_by_link_text("Podcast \"IT talks with Comarch\"").click()
        driver.find_element_by_link_text("Wydarzenia, konferencje").click()
        driver.find_element_by_xpath("//div[@id='input1e_chosen']/a/span").click()
        driver.find_element_by_xpath("//div[@id='input1e_chosen']/div/ul/li[2]").click()
        driver.find_element_by_xpath("//div[@id='input1e_chosen']/a").click()
        driver.find_element_by_xpath("//div[@id='input1e_chosen']/div/ul/li[3]").click()
        driver.find_element_by_xpath("(//a[contains(text(),'Kariera')])[2]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [selectWindow | win_ser_1 | ]]
        driver.find_element_by_link_text("Kontakt").click()
        driver.find_element_by_id("element000").click()
        driver.find_element_by_id("element000").clear()
        driver.find_element_by_id("element000").send_keys("Kamil Bartek")
        driver.find_element_by_id("element002").click()
        driver.find_element_by_id("element002").clear()
        driver.find_element_by_id("element002").send_keys("kamilbartek@wp.pl")
        driver.find_element_by_id("element010").click()
        # ERROR: Caught exception [ERROR: Unsupported command [selectFrame | index=1 | ]]
        driver.find_element_by_xpath("//div[4]/div/div").click()
        # ERROR: Caught exception [ERROR: Unsupported command [selectFrame | relative=parent | ]]
        driver.find_element_by_id("element010").click()
        driver.find_element_by_id("element010").clear()
        driver.find_element_by_id("element010").send_keys("asd")
        driver.find_element_by_id("go-forward-button").click()
        driver.find_element_by_xpath("(//img[@alt='Close'])[2]").click()
        driver.find_element_by_id("element000").click()
        driver.find_element_by_id("element000").clear()
        driver.find_element_by_id("element000").send_keys("Bartek Slimak")
        driver.find_element_by_id("element002").clear()
        driver.find_element_by_id("element002").send_keys("bartekslimak2@onet.pl")
        driver.find_element_by_id("element010").click()
        driver.find_element_by_id("element010").clear()
        driver.find_element_by_id("element010").send_keys("testq")
        driver.find_element_by_id("go-forward-button").click()
        driver.find_element_by_xpath("(//img[@alt='Close'])[2]").click()
        driver.close()
        # ERROR: Caught exception [ERROR: Unsupported command [selectWindow | win_ser_local | ]]

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        # To know more about the difference between verify and assert,
        # visit https://www.seleniumhq.org/docs/06_test_design_considerations.jsp#validating-results
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()

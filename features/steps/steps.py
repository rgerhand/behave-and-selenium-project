import time
from behave import given, when, then
from selenium.webdriver.common.action_chains import ActionChains


@given(u'Open the website')
def step_open_website(context):
    context.driver.get('https://www.comarch.pl/')


@when(u'I test slider')
def step_test_slider(context):
    context.driver.find_element_by_xpath("//aside/div/div[2]").click()
    context.driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div[2]/ul").click()
    context.driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div[2]/ul/span[4]").click()
    context.driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div[2]/ul/span[5]").click()
    context.driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div/i[2]").click()
    context.driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div/i").click()
    context.driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div/i").click()
    context.driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div/i").click()
    context.driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div/i").click()
    context.driver.find_element_by_xpath("//div[@id='widget-6']/div/div/div[2]/ul/span").click()


@then(u'I get Zobacz demo button')
def step_get_demo_button(context):
    context.driver.find_element_by_link_text("Zobacz demo")
    assert context.driver.find_element_by_link_text("Zobacz demo")


@given(u'Find the podcasts subpage')
def step_find_podcasts_subpage(context):

    element = context.driver.find_element_by_link_text("FIRMA")
    action = ActionChains(context.driver)
    action.move_to_element(element).perform()


@when(u'I find podcasts list')
def step_find_podcasts_list(context):
    context.driver.find_element_by_link_text(u'Podcast "IT talks with Comarch"')


@then(u'I open podcast by title')
def step_open_podcast_by_title(context):
    context.driver.find_element_by_link_text(u'Podcast "IT talks with Comarch"').click()
    context.driver.find_element_by_link_text(u'Seria podcastów IT Talks with Comarch')
    assert context.driver.find_element_by_link_text(u'Seria podcastów IT Talks with Comarch')


@given(u'Find the events subpage')
def step_find_events_subpage(context):
    firma = context.driver.find_element_by_link_text("FIRMA")
    aktualnosci = context.driver.find_element_by_link_text("Aktualności")
    action = ActionChains(context.driver)
    action.move_to_element(firma).perform()
    action.move_to_element(aktualnosci).click().perform()


@when(u'I check events by category')
def step_check_events_by_category(context):
    context.driver.find_element_by_xpath("//div[@id='input1e_chosen']/a/span").click()
    context.driver.find_element_by_xpath("//div[@id='input1e_chosen']/div/ul/li[2]").click()
    context.driver.find_element_by_xpath("//div[@id='input1e_chosen']/a").click()
    context.driver.find_element_by_xpath("//div[@id='input1e_chosen']/div/ul/li[3]").click()


@then(u'I get Finanse i Controlling text')
def step_get_fic_text(context):
    context.driver.find_element_by_xpath("//li[2]/div/a/div/h3")
    assert context.driver.find_element_by_xpath("//li[2]/div/a/div/h3")


@given(u'Find the contact subpage')
def step_find_contact_subpage(context):
    context.driver.find_element_by_xpath("(//a[contains(text(),'Kontakt')])[2]").click()


@when(u'I input {firstname} and {lastname} and {email} and {company} and {message}')
def step_input_personal_data(context, firstname, lastname, email, company, message):
    context.driver.find_element_by_id("element000").send_keys(firstname)
    context.driver.find_element_by_id("element001").send_keys(lastname)
    context.driver.find_element_by_id("element002").send_keys(email)
    context.driver.find_element_by_id("element004").send_keys(company)
    context.driver.find_element_by_xpath("//form[@id='form-linkedin']/div/div/div").click()
    context.driver.find_element_by_link_text("Kraj*").click()
    context.driver.find_element_by_xpath("(//input[@type='text'])[12]").click()
    context.driver.find_element_by_xpath("//div[@id='element008_chosen']/div/ul/li[155]").click()
    context.driver.find_element_by_link_text("Rodzaj zapytania*").click()
    context.driver.find_element_by_xpath("//div[@id='element0010_chosen']/div/ul/li[6]").click()
    context.driver.find_element_by_id("element0012").send_keys(message)
    context.driver.find_element_by_name("form-navi[SUBMIT]").click()


@then(u"I send the form")
def step_send_form_request(context):
    context.driver.find_element_by_xpath("//button").click()
    assert context.driver.find_element_by_name("form-navi[SUBMIT]")

Feature: Website
  @slow
  Scenario:Website slider test
    Given Open the website
    When I test slider
    Then I get Zobacz demo button

  Scenario: Podcasts subpage test
    Given Find the podcasts subpage
    When I find podcasts list
    Then I open podcast by title

  Scenario: Podcasts events test
    Given Find the events subpage
    When I check events by category
    Then I get Finanse i Controlling text

  @wip
  Scenario Outline: Complete the form
    Given Find the contact subpage
    When I input <firstname> and <lastname> and <email> and <company> and <message>
    Then I send the form

    Examples:
      | firstname |  lastname | email           | company       |  message  |
      | Kamil     |  Bartek   | asd@wp.pl      | Kamil Bartek  |  asd      |
      | Bartek    |  Slimak   | testmail@wp.pl  | BartekCompany |  test     |
